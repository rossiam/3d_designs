// This little file generates printable boxes which can be divided up into trays.

// Use the box_and_lid module to generate them. The two parameters are:
//   size: a simple vector that describes outer dimensions of the box (*not* including the lid)
//   trays: an array of arrays
//     - each element of the array describes a single row of trays and is an array of a number and an array
//     - the number defines the depth of the row and the array defines the widths of the trays in the row
//     - the last row will get all of the remaining space, regardless of whatever value you set its
//       depth to. I like to set it to 1.
//     - Similarly, the last tray in a row gets the rest of the row.

// Sometimes you don't want a lid.
// Sometimes you want to make a box that's so big you need to print the lid and base separately.
generate_box = 1;
generate_lid = 1;

// Read on for some examples.

// A simple example using the default random size I chose and no separate trays.
//box_and_lid();

// a small approximately 7 x 6.5 x 2 cm box with two trays
/*
box_and_lid(
	size = [70, 65, 18.7],
	trays = [
		[1, [(70-3*wt)/2, 1]], // one row with two equally sized trays
	]
);
*/

// A large box to hold lots of random small things. I created this one to store jumper wires.
box_and_lid(
	size = [200, 130, 15],
	trays = [ // this box has for rows of trays
		[31.5, [40, 40, 26.4, 21, 21, 21, 1]], // first row; 3.15 cm deep with 7 trays
		[31.5, [40, 40, 30, 30, 25.8, 1]], // lots more small trays!
		[31.5, [110, 1]], // only two in this row
		[1, [135, 1]], // last row uses up the rest of the depth and has two trays
	]
);

// Another random example.
/*
box_and_lid(
	size = [135 + wt2, 4 * (30 + wt) + wt, 15],
	trays = [
		[30, [60, 35, 1]], // first row has three trays, 6 cm, 3.5 cm and the rest of the width.
		[30, [85, 30]],
		[30, [110, 1]],
		[30, [1]], // last row is only one tray
	]
);
*/

// Settings you don't need to change as much.

// tolerance (extra space to make things fit)
t = 0.2;
t2 = t * 2;

// wall thickness
wt = 1.2;
wt2 = wt * 2;
chamfer_radius = 0.4;

lid_depth = 5;  // full height of lid
lid_trench_depth = 1.6; // depth of "trench" that helps keep parts from jumping trays


module box_and_lid(size = [60, 40, 15], trays = [[1, [1]]])
{
	if (generate_box) divided_box(size, trays);
	if (generate_lid) translate([-wt - t, size[1] + 2, 0]) divided_lid(size, trays);
//	translate([size[0] + 2, -wt - t, 0]) divided_lid(size, trays);
}


function tail(vector) = [for (index = [1:len(vector)-1]) vector[index]];

module chamfered_box(size, chamfer_top = false) {
	cr2 = 2 * chamfer_radius;
	hull() {
		translate([chamfer_radius, chamfer_radius, 0]) cube([size[0]-cr2, size[1]-cr2,0.1]);
		if (chamfer_top) {
			translate([0, 0, chamfer_radius]) cube([size[0], size[1], size[2]-2*chamfer_radius]);
			translate([chamfer_radius, chamfer_radius, size[2]-0.1]) cube([size[0]-cr2, size[1]-cr2,0.1]);
		} else {
			translate([0, 0, chamfer_radius]) cube([size[0], size[1], size[2]-chamfer_radius]);
		}
	}
}

// cuts trays in a single row
module cut_tray_row(trays, size) {
	this_width = len(trays) == 1 ? size[0] : trays[0];
	echo(tray=this_width);
	leftover_width = size[0] - this_width - wt;
	chamfered_box([this_width, size[1], size[2]]);
	if (len(trays) > 1) {
		translate([this_width + wt, 0, 0]) cut_tray_row(tail(trays), [leftover_width, size[1], size[2]]);
	}
}

// cuts row of trays
module cut_tray_rows(tray_rows, size) {
	tray_row = tray_rows[0];
	this_depth = len(tray_rows) == 1 ? size[1] : tray_row[0];
	echo(ROW=this_depth);
	leftover_depth = size[1] - this_depth - wt;
	cut_tray_row(tray_row[1], [size[0], this_depth, size[2]]);
	if (len(tray_rows) > 1) {
		translate([0, this_depth + wt, 0])
			cut_tray_rows(tail(tray_rows), [size[0], leftover_depth, size[2]]);
	}
}

module divided_box(size, tray_rows) difference() {
	chamfered_box(size, true);

	inside_size = size - [wt2, wt2, 0];
	translate([wt, wt, wt]) cut_tray_rows(tray_rows, inside_size);
}

module lid_box_row(trays, size) {
	this_width = len(trays) == 1 ? size[0] : trays[0];
	leftover_width = size[0] - this_width - wt;
	translate([t, t, -0.1]) difference() {
		cube([this_width - t2, size[1] - t2, lid_trench_depth + 0.1]);
		translate([wt, wt, -0.1]) cube([this_width - t2 - wt2, size[1] - t2 - wt2, lid_trench_depth + 0.3]);
	}
	if (len(trays) > 1) {
		translate([this_width + wt, 0, 0]) lid_box_row(tail(trays), [leftover_width, size[1], size[2]]);
	}
}

module lid_box_rows(tray_rows, size) {
	tray_row = tray_rows[0];
	this_depth = len(tray_rows) == 1 ? size[1] : tray_row[0];
	leftover_depth = size[1] - this_depth - wt;
	lid_box_row(tray_row[1], [size[0], this_depth, size[2]]);
	if (len(tray_rows) > 1) {
		translate([0, this_depth + wt, 0])
			lid_box_rows(tail(tray_rows), [size[0], leftover_depth, size[2]]);
	}
}

module divided_lid(size, tray_rows) translate([size[0] + wt2 + t2, 0, 0]) mirror() {
	difference() {
		chamfered_box([size[0] + wt2 + t2, size[1] + wt2 + t2, lid_depth + t]);

		translate([wt, wt, wt + t]) 
			chamfered_box([size[0] + t2, size[1] + t2, lid_depth + t]);
	}

	inside_size = size - [wt2, wt2, 0];
	translate([wt2 + t, wt2 + t, wt]) lid_box_rows(tray_rows, inside_size);
}
