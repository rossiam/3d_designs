blade_disposal();

module chamfered_box(dims, r) hull() {
	d2 = dims - [r*2, r*2, r*2];
	translate([r, r, 0]) cube(dims - [r*2, r*2, 0]);
	translate([r, 0, r]) cube(dims - [r*2, 0, r*2]);
	translate([0, r, r]) cube(dims - [0, r*2, r*2]);
}

module blade_disposal(outside = [20, 50, 60], wall_thickness = 3) difference() {
	chamfered_box(outside, 0.6);
	wt = wall_thickness;
	difference() {
		translate([wt, wt, wt]) cube(outside - [wt*2,wt*2,wt*2]);
		// keep a little "catch" inside to make it harder to get blades out
		zz = 0.9;
		translate([(outside[0] - 1.8)/2 - zz, (outside[1] - 20.2)/2 - zz, wt - 0.1]) difference() {
			cube([1.8 + zz * 2, 20.1 + zz * 2, 1.9]);
			translate([zz, zz, -0.1]) cube([1.8, 20.1, 10]);
		}
	}
	translate([(outside[0] - 1.8)/2, (outside[1] - 20.2)/2, -1]) blade_slot(wall_thickness + 2);
}

module blade_slot(t) {
	// 1.3 x  12.7, 1.8 x 6.4 overall 1.8 x 20.2
	translate([(1.8-1.3)/2, 0, 0]) cube([1.3, 14.8, t]);
	translate([0, 13.8, 0]) cube([1.8, 6.4, t]);
}
