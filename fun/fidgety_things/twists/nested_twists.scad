// The overall height of the outer ring (or all rings for rings that aren't closed).
// This number should be multiple of layer_height; i.e. height / layer_height should be an integer.
height = 80;

// The radius of the circle with ring numbered 1 (the "outside" ring.).
outer_radius = 20;

// Set the layer height you plan to print with. Slices of this thickness
// will be used when generating the models.
// (TODO: maybe add first layer height too.)
layer_height = 0.2; // [0.15, 0.2];

// Rings are numbered from the outside in. So, ring number 1 is the ring with the dimensions
// specified above. Ring number 2 will fit just inside it and so on. If you've printed
// a few rings and decide you want on that just fits around 1, you can use 0. Negative numbers
// work as expected as well.
ring_num = 1;

// The thickness of the wall for each twist. I have found increasing this some for open-ended
// twists helps them stick to the bed a little better. One could also reduce or remove the
// chamfering. If you're making twists with a closed end, make sure this is a multple of the
// layer height for best results.
wt = 2;

// A little clearance between the twists. Make this number a little bigger if you have
// trouble getting them to slide easily.
t = 0.3;

// Vertical clearance. This is only used for twists with a closed end. Ideally this would
// be a multiple of the layer height.
tv = layer_height; // vertical clearance

// The angle at which the shape should rotate over the entire height (given by "height" above).
twist = 240;

// The number of sides the shape should have.
sides = 5;

// The rounding radius for the corners of the shape.
rounding = 2;

// A little chamfer helps fit the twists together more easily. Ideally this should be
// a multiple of the layer height 1/3 of the wall thickness (wt) or less. Keep this
// small (or set to zero) and/or make the wall thickness (wt) a little bigger for
// open ended twists (close_bottom = false).
chamfer_r = layer_height * 3;

// Set to true to close one end of the twists. They are a little harder to print without
// this and they tend to fall all over the place when you try to pick them up.
close_bottom = true;

// Set this to tru to include a little air hole in closed end of each peice (unused if
// close_bottom is false).
include_air_hole = true;

// The size of the air hole in the closed end of each peice.
air_hole_d = 5;



twisted_ring(ring_num);

// From OpenSCAD examples:
module rounded_ngon(num, r, rounding = 0) {
  function v(a) = let (d = 360/num, v = floor((a+d/2)/d)*d) (r-rounding) * [cos(v), sin(v)];
  polygon([for (a=[0:10:360-1]) v(a) + rounding*[cos(a),sin(a)]]);
}

module chamfered_twist(r, h = height, tw = twist, invert_top_chamfer = false, invert_bottom_chamfer = false) {
	z1 = chamfer_r;
	z2 = h - chamfer_r;
	h_main = h - 2 * chamfer_r;
	tw_chamfer = tw * z1 / h;
	tw_main = tw * (z2 - z1) / h;

	slices = h_main / layer_height; // low-res: / 4;
    echo(h=h, h_main=h_main, slices=slices);

	scale_bottom = invert_bottom_chamfer ? (r - chamfer_r) / r : r / (r - chamfer_r);
	scale_top = invert_top_chamfer ? r / (r - chamfer_r) : (r - chamfer_r) / r;
	union() {
		linear_extrude(height = chamfer_r, twist = tw_chamfer, slices = chamfer_r / layer_height, scale = scale_bottom) rounded_ngon(sides, r - (invert_bottom_chamfer ? - chamfer_r : chamfer_r), rounding);
		rotate([0, 0, - tw_chamfer]) translate([0, 0, z1]) linear_extrude(height = h_main, twist = tw_main, slices = slices) rounded_ngon(sides, r, rounding);
		rotate([0, 0, - tw_main - tw_chamfer]) translate([0, 0, z2]) linear_extrude(height = chamfer_r, twist = tw_chamfer, slices = chamfer_r / layer_height, scale = scale_top) rounded_ngon(sides, r, rounding);
	}
}

module chamfered_ring(r, h = height, tw = twist) difference() {
	echo(r=r, h=h, tw=tw);
	chamfered_twist(r, h, tw);
	if (close_bottom) {
		translate([0, 0, wt])
			rotate([0, 0, - tw * wt / h]) chamfered_twist(r - wt, h - wt, tw * (h - wt) / h, invert_top_chamfer = true);
        if (include_air_hole) translate([0, 0, -0.1]) cylinder(d = air_hole_d, h = wt + 0.2, $fn = 24);
	} else {
		chamfered_twist(r - wt, h, tw, invert_top_chamfer = true, invert_bottom_chamfer = true);
	}
}

module twisted_ring(ring_to_render, or = outer_radius) {
	h = height - (close_bottom ? (wt + tv) * (ring_to_render - 1) : 0);
	tw = twist * h/height;
	chamfered_ring(or - (ring_to_render - 1) * (wt + t), h, tw);
}
