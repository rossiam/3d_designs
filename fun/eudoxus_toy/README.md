# Eudoxus Toy

This was inspired by [gianfranco crevani's](http://www.thingiverse.com/gianfranco/about) [gyroscopic relaxing keyring](http://www.thingiverse.com/thing:1307100). I wanted to be able to add more rings and change the size of them so I made my own version.

Named after [Eudoxus of Cnidus](https://en.wikipedia.org/wiki/Eudoxus_of_Cnidus) because why not?

Build your own version using the `eudoxus_toy` module and changing the parameters.  See the file for more details.
