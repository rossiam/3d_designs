// Create a sphere toy by calling the eudoxus_toy module defined
// below. This module takes the following arguments:

// outside_radius:  the radius of the largets sphere
// thickness:  how thick should the toy be
// num_rings:  how many rings to make (this does not include the
//   inside if one is specified
// ring_thickness:  how thick to make each ring
// ring_spacing:  a little space between the rings so they spin easily
// inside_style:  "solid", "none" or "hole"
//   solid - just a solid sphere in the middle
//   none - no iside piece
//   hole - leave a straight hole in the middle
// When "inside_style" is "hole" these options can be used:
//   inside_hole_radius:  the radius a hole
//   inside_hole_sides:  number of sides for inside hole


// Here are some examples:

// Fancy complicated use-all-the-options example. This one is rather large.
/*
eudoxus_toy(
	outside_radius = 50,
	thickness = 35,
	num_rings = 4,
	ring_thickness = 6,
	ring_spacing = 0.4,
	inside_style = "hole",
	inside_hole_radius = 8,
	inside_hole_sides = 6
);
*/

// Exmaple using all defaults. Medium sized-with lots of rings.
eudoxus_toy();

// Example just tweaking a few things. Smaller, thicker and only three rings.
/*
eudoxus_toy(
	outside_radius = 18,
	thickness = 20,
	num_rings = 3,
	ring_thickness = 2
);
*/


$fn = 120;

module eudoxus_toy(
	outside_radius = 24.5,
	thickness = 18,
	num_rings = 5,
	ring_thickness = 2.4,
	ring_spacing = 0.4,
	inside_style = "none", // "solid", "none" or "hole"
	inside_hole_radius = 3.1, // if inside_style == "hole"
	inside_hole_sides = $fn // number of sides for inside hole if inside_style == "hole"
) translate([0, 0, thickness / 2]) difference() {
	ring_space = ring_thickness + ring_spacing;
	union() {
		for (ring_num = [0 : num_rings - 1]) {
			ring_inner_radius = outside_radius - ring_thickness - ring_num * ring_space;
			ring_outer_radius = outside_radius - ring_num * ring_space;
			ring(ring_outer_radius, ring_inner_radius);
		}
		if (inside_style == "solid") {
			sphere(outside_radius - num_rings * ring_space);
		} else if (inside_style == "hole") {
			difference() {
				sphere(outside_radius - num_rings * ring_space);
				translate([0, 0, -thickness/2-1]) cylinder(r = inside_hole_radius, h = thickness + 2, $fn = inside_hole_sides);
			}
		} else if (inside_style != "none") {
			echo("<font color=red>Unknown inside_style </font>", inside_style);
		}
	}
	translate([-outside_radius, -outside_radius, 0]) {
		cutoff_cube_dimensions = [2 * outside_radius + 2, 2 * outside_radius + 2, outside_radius + 1];
		translate([0, 0, thickness/2]) cube(cutoff_cube_dimensions);
		translate([0, 0, -thickness/2 - outside_radius - 1]) cube(cutoff_cube_dimensions);
	}
}

module ring(outer_radius, inner_radius) difference() {
	sphere(r = outer_radius);
	sphere(r = inner_radius);
}
