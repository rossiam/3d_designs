mode = "test"; // base, test, face

size = 30;
color_plate_offset = 2;
color_plate_size = size - color_plate_offset * 2;
color_plate_thickness = 1.2;

cr = 0.4; // chamfer radius

wt = 2; // thickness of "wall" pin clips into
t = 0.2; // tolerance
pin_depth = 7; // overall height of a pin
pin_head_depth = 2.4; // overall height of a pin head
pin_r = 2.4; // raidus of the base of a pin
pin_slot_w = 1.2; // width of slot to cut out of pin
pin_r2 = 2.8; // raidus of pin head
pin_r3 = 2; // very top radius of pin (chamfer at top is a little bigger than bottom

// The pin offset from corner/edge. If we put it too close to the edge, they will interfere with each other.
// (We probably don't need 4 pins so there's probably room for improvement here.)
pin_offset = max(pin_r2 + color_plate_offset + 3, pin_depth + pin_r2 + 3 * t);

pin_hole_r = 2.74; // main radius of pin hole
pin_h_c = 0.25; // pin hole chamfer



$fn = 120;

module pin() difference() {
	union() {
		translate([0, 0, -0.1]) cylinder(r = pin_r, h = pin_depth - (pin_r2 - pin_r3));
		translate([0, 0, pin_depth - pin_head_depth]) hull() {
			cylinder(r1 = pin_r, r2 = pin_r2, h = pin_r2 - pin_r);
			translate([0, 0, pin_head_depth - (pin_r2 - pin_r3)])
				cylinder(r1 = pin_r2, r2 = pin_r3, h = pin_r2 - pin_r3);
		}
	}
	translate([-pin_slot_w/2, -pin_r2-0.1, 0]) cube([pin_slot_w, pin_r2 * 2 + 0.2, pin_depth + 0.1]);
}

module pin_hole(is_bottom = false) {
	pin_hole_r2 = pin_hole_r + pin_h_c + 0.1;
	depth = pin_depth - pin_head_depth + (pin_r2 - pin_r);
	translate([0, 0, -0.1]) cylinder(r1 = pin_hole_r2, r2 = pin_hole_r, h = pin_h_c + 0.1);
	translate([0, 0, 0.1]) cylinder(r = pin_hole_r, h = depth);
	translate([0, 0, depth - pin_h_c]) cylinder(r1 = pin_hole_r, r2 = pin_hole_r2, h = pin_h_c + 0.1);
	translate([0, 0, depth + 0.1]) cylinder(r = pin_hole_r2, h = pin_head_depth + t - pin_h_c - 0.1);
	if (is_bottom) translate([0, 0, pin_depth + t + 0.1]) cylinder(r = pin_hole_r2, r2 = 0.2, h = pin_hole_r2 - 0.2);
}

module pins() {
	translate([pin_offset, pin_offset, 0]) rotate([0, 0, -45]) pin();
	translate([size - pin_offset, pin_offset, 0]) rotate([0, 0, 45]) pin();
	translate([pin_offset, size - pin_offset, 0]) rotate([0, 0, 45]) pin();
	translate([size - pin_offset, size - pin_offset, 0]) rotate([0, 0, -45]) pin();
}

module pin_holes(is_bottom = false) {
	for (x = [pin_offset, size - pin_offset])
		for (y = [pin_offset, size - pin_offset])
			translate([x, y, 0]) pin_hole(is_bottom);
}

module face_base() {
	translate([-color_plate_size/2, -color_plate_size/2, -size/2]) hull() {
		cps2 = color_plate_size - 2 * cr;
		translate([0, 0, cr]) cube([color_plate_size, color_plate_size, color_plate_thickness - cr]);
		translate([cr, cr, 0]) cube([cps2, cps2, 0.1]);
	}
}

module one_by_cube() difference() {
	union() {
		translate([-size/2, -size/2, -size/2]) hull() {
			s2 = size - 2 * cr;
			translate([cr, cr, 0]) cube([s2, s2, size]);
			translate([0, cr, cr]) cube([size, s2, s2]);
			translate([cr, 0, cr]) cube([s2, size, s2]);
		}
		translate([0, 0, color_plate_thickness]) rotate([180, 0, 0]) face_base();
	}

	rotate([-90, 0, 0]) translate([-size/2, -size/2, -size/2]) pin_holes();
	rotate([90, 0, 0]) translate([-size/2, -size/2, -size/2]) pin_holes();
	rotate([0, -90, 0]) translate([-size/2, -size/2, -size/2]) pin_holes();
	rotate([0, 90, 0]) translate([-size/2, -size/2, -size/2]) pin_holes();
	translate([-size/2, -size/2, -size/2]) pin_holes(is_bottom = true);
}

module face() {
	face_base();
	translate([-size/2, -size/2, -size/2 + color_plate_thickness]) pins();
}


if (mode == "test") {
	difference() {
		one_by_cube();
		translate([-size, -size, size/2 - pin_offset]) cube([size, size, size]);
		translate([0, size/2 - pin_offset, -size/2 - 0.1]) cube([size, size, size + color_plate_thickness + 0.2]);
	}
	translate([size + 5, 0, 0]) face();
} else if (mode == "base") {
	one_by_cube();
} else if (mode == "face") {
	face();
}
