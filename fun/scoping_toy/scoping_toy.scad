// Exmaple using all defaults.
scoping_toy();

// Larger.
//scoping_toy(outside_radius = 45, height = 90, num_sections = 12);

// So huge it's a little silly. I have not tried printing this one!
//scoping_toy(outside_radius = 60, height = 120, num_sections = 16);

// Mini-ish. I use this one to test tolerances.
//scoping_toy(outside_radius = 16, num_sections = 3, height = 27);


// If you wnat to see what it looks like "inside" (I used this for debugging):
/*
difference() {
	scoping_toy();
	translate([-50, 0, -1]) cube([100, 100, 100]);
	translate([-50, -101, -1]) cube([100, 100, 100]);
}
*/


$fn = 120;

// The "outside_radius" does not include the lip around the bottom so including
// this lip, the maximum radius is outer_radius + overlap.
module scoping_toy(
	outside_radius = 30,
	height = 60,
	num_sections = 6,
	wall_thickness = 1.2,
	overlap = 1.2,
	overlap_height = 0.6,
	tolerance = 0.3)
{
	for (section_num = [1 : num_sections]) {
		radius = outside_radius - (section_num - 1) * (wall_thickness + overlap + tolerance);
		section_height = height - (num_sections - section_num) * (overlap_height + overlap + tolerance);
		section(radius, wall_thickness, section_height, overlap, overlap_height, tolerance);
	}
}

// The radius is the outside radius of main body (not including lip around top and bottom).
module section(radius, wall_thickness, height, overlap, overlap_height, tolerance) difference() {
	union() {
		// main body
		cylinder(r = radius, h = height);

		// bottom lip 
		cylinder(r = radius + overlap, h = overlap_height);
		translate([0, 0, overlap_height]) cylinder(r1 = radius + overlap, r2 = radius, h = overlap);

		// top outside lip
		translate([0, 0, height - overlap_height - overlap]) cylinder(r1 = radius, r2 = radius + overlap, h = overlap);
		translate([0, 0, height - overlap_height])
			cylinder(r = radius + overlap, h = overlap_height + 0.1);
	}
	translate([0, 0, -0.1]) hull() {
		cylinder(r = radius - wall_thickness, h = height - overlap_height - overlap + 0.1);
		cylinder(r = radius - wall_thickness - overlap, h = height - overlap_height - 0.1);
	}
	cylinder(r = radius - wall_thickness - overlap, h = height + 1);
	translate([0, 0, -1]) cylinder(r = radius - wall_thickness - overlap, h = height + 2);
}
