// Exmaple using all defaults.
// scoping_toy();

// Smaller.
// scoping_toy(outside_radius = 18, outside_height = 50, num_sections = 4, twist = 120);

// Taller
// scoping_toy(outside_height = 100, twist = 180);

// Larger.
// scoping_toy(outside_radius = 50, outside_height = 100, num_sections = 11, twist = 120);

// Ludicrous
// scoping_toy(outside_radius = 60, outside_height = 120, twist = 120, num_sections = 12);

// Obscene. So huge it's a silly. I have not tried printing this one since it needs about
// 1.6 kg of filament and 6 days to print
// scoping_toy(outside_radius = 75, outside_height = 150, twist = 120, num_sections = 16);

// Mini-ish. I use this one to test gaps.
//scoping_toy(outside_radius = 16, num_sections = 3, outside_height = 27);


// If you want to see what it looks like "inside" (I used this for debugging):
/*
difference() {
	scoping_toy();
	translate([-50, 0, -1]) cube([100, 100, 100]);
	translate([-50, -101, -1]) cube([100, 100, 100]);
}
*/

// Test
difference() {
	scoping_toy(outside_height = 50, twist = 120, num_sections = 3, fill_in_center = true,
		include_handle = true);
	translate([0, 0, -0.1]) cube([50, 50, 100.2]);
}

lh = 0.2; // layer height you plan to print at

$fn = 72;

// The "outside_radius" does not include the lip around the bottom so including
// this lip, the maximum radius is outer_radius + overhang.
module scoping_toy(
	outside_radius = 30,
	outside_height = 60, // height of biggest or outside ring
	overall_height = -1, // overall height; overrides outside_height if set
	twist = 60, // twist in degrees of outside ring; inner rings will twist slightly more
	num_sections = 6, // the number of "rings" to make
	wall_thickness = 1.8,
	overhang = 1.2, // amount of overhang that keeps peices together
	lip_height = 0.6, // top and bottom lips
	gap = 0.3, // amount of extra space so parts don't print fused together
	fill_in_center = false,
	include_handle = false,
	handle_diameter = 10,
	handle_length = 50,
	$fn = 6)
{
	height = overall_height == -1
		? outside_height + (num_sections - 1) * (lip_height + overhang + gap)
		: overall_height;
	echo(height=height);

	overall_twist = twist * height / (height - (num_sections - 1) * (lip_height + overhang + gap));

	for (section_num = [1 : num_sections]) {
		fill_in = section_num == num_sections && fill_in_center;
		echo(fill_in=fill_in);
		radius = outside_radius - (section_num - 1) * (wall_thickness + overhang + gap);
		section_height = height - (num_sections - section_num) * (lip_height + overhang + gap);
		echo(section_height=section_height);
		section_twist = overall_twist * section_height / height;
		section(radius, wall_thickness, section_height, section_twist, overhang, lip_height, gap, fill_in);
	}

	if (include_handle) up(height - 0.1)
		cylinder(d = handle_diameter, h = handle_length, $fn = 72);
}

module up(z_height) translate([0, 0, z_height]) children();
module around(angle) rotate([0, 0, -angle]) children();

// The radius is the outside radius of main body (not including lip around top and bottom).
module section(radius, wall_thickness, height, twist, overhang, lip_height, gap, fill_in) {
	z1 = 0;
	z2 = lip_height;
	z3 = z2 + overhang;
	z5 = height - lip_height;
	z4 = z5 - overhang;

	t1 = twist * z2 / height;
	t2 = twist * (z3 - z2) / height;
	t3 = twist * (z4 - z3) / height;
	t4 = t2;
	t5 = t1;

	ol_slices = round((z3 - z2) / lh);

	// bottom lip
	linear_extrude(height = lip_height, twist = t1, slices = round(z2 / lh)) difference() {
		circle(r = radius + overhang);
		if (!fill_in) circle(r = radius - wall_thickness);
	}
	// bottom chamfer
	up(lip_height) around(t1) {
		difference() {
			scale1 = radius / (radius + overhang);
			linear_extrude(height = overhang, twist = t2, slices = ol_slices, scale = scale1)
				circle(r = radius + overhang);
			if (!fill_in) linear_extrude(height = overhang, twist = t2, slices = ol_slices)
				circle(r = radius - wall_thickness);
		}

		// main body
		up(overhang) around(t2) {
			linear_extrude(height = z4 - z3, twist = t3, slices = round((z4 - z3) / lh)) difference() {
				circle(r = radius);
				if (!fill_in) circle(r = radius - wall_thickness);
			}

			// top chamfer
			up(z4 - z3) around(t3) {
				difference() {
					scale1 = (radius + overhang) / radius;
					linear_extrude(height = overhang, twist = t4, slices = ol_slices, scale = scale1)
						circle(r = radius);
					scale2 = (radius - wall_thickness - overhang) / (radius - wall_thickness);
					if (!fill_in) linear_extrude(height = overhang, twist = t4, slices = ol_slices, scale = scale2)
						circle(r = radius - wall_thickness);
				}
				// top lip
				up(overhang) around(t4)
					linear_extrude(height = lip_height, twist = t5, slices = round(z2 / lh)) difference() {
						circle(r = radius + overhang);
						if (!fill_in) circle(r = radius - wall_thickness - overhang);
					}
			}
		}
	}
}
