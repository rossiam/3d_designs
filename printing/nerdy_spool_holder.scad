$fn = 120;

// boards 19 x 38

board_height = 40;
board_thickness = 9.2; // make this bigger if fit on board doesn't fit

holder_len = 95;
holder_width = 14;
holder_d = 25;
keep_on_d = 40;
keep_on_len = 10;

grabber_thickness = 5;
grabber_lip = 2;


module chamfered_box(size, chamfer_size = 0.4) hull() {
	cs2 = chamfer_size * 2;
	translate([chamfer_size, chamfer_size, 0]) cube(size - [cs2, cs2, 0]);
	translate([chamfer_size, 0, chamfer_size]) cube(size - [cs2, 0, cs2]);
	translate([0, chamfer_size, chamfer_size]) cube(size - [0, cs2, cs2]);
}

// A test shelf bracket to hold boards for testing.
module fake_shelf_bracket() difference() {
	union() {
		cube([150 + board_thickness * 2 + 4 * 2, 10, 16]);
		for (x = [0, 150 + board_thickness * 2 + 4 * 2]) for (y = [0, 10]) translate([x, y, 0]) cylinder(d = 7, h = 0.6);
	}
	translate([0, -0.1, 3]) {
		for (x = [4, 4 + 150 + board_thickness]) translate([x, 0, 0]) cube([board_thickness, 10.2, 13.1]);
		translate([2 * 4 + board_thickness, 0, 0]) cube([150 - 2 * 4, 10.2, 13.1]);
	}
}

module nerdy_spool_holder() {
	shift = 12;
	vertical_offset = 7;
	translate([0, 0, board_height + grabber_thickness * 2 - holder_d / 2]) difference() {
		intersection() {
			translate([-holder_width/2, - holder_len - keep_on_len - 1, -holder_d]) cube([holder_width, holder_len + keep_on_len + 3, holder_d * 2]);
			rotate([90, 0, 0]) translate([0, 0, -2]) {
				cylinder(h = holder_len + 2, d = holder_d);
				translate([0, 0, holder_len]) intersection() {
					cylinder(d = keep_on_d, h = keep_on_len);
					translate([-keep_on_d/2, 2, 0]) hull() {
						cube([keep_on_d, 20, keep_on_len]);
						translate([0, - keep_on_len * 2 * vertical_offset / shift, 0]) cube([keep_on_d, 0.1, 0.1]);
					}
				}
			}
		}
		for (index = [0:5]) hull() {
			d = 9;
			y = -holder_len + index * (d + 5) - 2;
			translate([- holder_width / 2 - 0.1, y + shift, -vertical_offset]) cube([holder_width + 0.2, d, 0.1]);
			translate([- holder_width / 2 - 0.1, y, vertical_offset - 0.1]) cube([holder_width + 0.2, d, 0.1]);
		}
	}
	translate([-holder_width/2, 0, 0]) difference() {
		chamfered_box([holder_width, board_thickness + 2 * grabber_thickness, board_height + 2 * grabber_thickness]);
		translate([-0.1, grabber_thickness, grabber_thickness]) cube([holder_width + 0.2, board_thickness, board_height]);
		hull() {
			translate([-0.1, -0.1, -0.1]) cube([holder_width + 0.2, board_thickness + grabber_thickness * 1.5, 0.1]);
			translate([-0.1, -0.1, grabber_thickness - 0.1 - 1]) cube([holder_width + 0.2, board_thickness + grabber_thickness - grabber_lip, 0.1]);
		}
		translate([-0.1, -0.1, grabber_thickness - 0.1 - 1]) cube([holder_width + 0.2, board_thickness + grabber_thickness - grabber_lip, 1.2]);
	}
}


rotate([0, 90, 0])
nerdy_spool_holder();

//fake_shelf_bracket();
